# Notes

This folder contains all templates. Some of them can be untested, that one can find in the comment section of the file

## Helpful Commands

&nbsp;
&nbsp;

*  Create Stack (A template creates stack and stack creates AWS resources)

```
aws cloudformation create-stack --stack-name somename --template-body file://somefile.yml --region eu-west-1
```

* Delete Stack (This has no output)
```
aws cloudformation delete-stack --stack-name somename --region eu-west-1
```


